package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductControllwe {

    static final String APIBaseURL = "/apitechu/v1";

    @GetMapping(APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }
    @GetMapping(APIBaseURL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La ide del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)){
                System.out.println("Producto encontrado");
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La ide del producto a crear es " + newProduct.getDesc());
        System.out.println("La descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es" + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return new ProductModel();
    }
    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("update product");
        System.out.println("La id del producto es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId((product.getId()));
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id) {
        System.out.println("Patch product");
        System.out.println("La id del producto es " + id);
        System.out.println("La id del producto a actualizar es " + productData.getId());
        System.out.println("La descripción del producto a actualizar es " + productData.getDesc());
        System.out.println("El precio del producto a actualizar es " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = productInList;

                if (productData.getDesc() != null) {
                    System.out.println("Actualizando descripción del producto ");
                    productInList.setDesc(productData.getDesc());
                }
                if (productData.getPrice() > 0) {
                    System.out.println("Actualizando precio del producto ");
                    productInList.setPrice(productData.getPrice());
                }
            }
        }
        return result;
    }
    @DeleteMapping(APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("Borrando ");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for(ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto a borrar encontrado ");
                foundCompany = true;
                result = productInList;
            }
        }

        if (foundCompany == true) {
            System.out.println("Borrando producto ");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }
}
